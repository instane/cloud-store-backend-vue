const functions = require("firebase-functions");
const admin = require("firebase-admin");
const serviceAccount = require("./serviceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://cloud-331b2.firebaseio.com"
});

// //import firebase functions modules
// const functions = require('firebase-functions');
// //import admin module
// const admin = require('firebase-admin');
// admin.initializeApp(functions.config().firebase);

// Listens for new messages added to messages/:pushId
exports.pushNotification = functions.https.onRequest((req, res) => {
  console.log("Push notification event triggered");

  // //  Grab the current value of what was written to the Realtime Database.
  // var valueObject = event.data.val();

  // if(valueObject.photoUrl != null) {
  //   valueObject.photoUrl= "Sent you a photo!";
  // }

//   // Create a notification
//   const payload = {
//     notification: {
//       title: "Title",
//       body: "body",
//       sound: "default"
//     }
//   };

//   //Create an options object that contains the time to live for the notification and the priority
//   const options = {
//     priority: "high",
//     timeToLive: 60 * 60 * 24
//   };

  const message = {
    android: {
      ttl: 3600 * 1000, // 1 hour in milliseconds
      priority: 'normal',
      notification: {
        title: '$GOOG up 1.43% on the day',
        body: '$GOOG gained 11.80 points to close at 835.67, up 1.43% on the day.',
        icon: 'stock_ticker_update',
        color: '#f45342'
      }
    },
    topic: 'cloud-channel'
  };
    // .sendToTopic("pushNotifications", payload, options)

  return admin
    .messaging()
    .send(message)
    .then(response => {
      console.info("Successfully sent message:", response);
    })
    .catch(error => {
      console.error(error);
    });
});

// // // Create and Deploy Your First Cloud Functions
// // // https://firebase.google.com/docs/functions/write-firebase-functions
// //
// // exports.helloWorld = functions.https.onRequest((request, response) => {
// //  response.send("Hello from Firebase!");
// // });

// // Take the text parameter passed to this HTTP endpoint and insert it into the
// // Realtime Database under the path /messages/:pushId/original
// exports.sendPush = functions.https.onRequest((req, res) => {
//   const message = {
//     android: {
//       ttl: 3600 * 1000, // 1 hour in milliseconds
//       priority: "normal",
//       notification: {
//         title: "$GOOG up 1.43% on the day",
//         body:
//           "$GOOG gained 11.80 points to close at 835.67, up 1.43% on the day.",
//         icon: "stock_ticker_update",
//         color: "#f45342"
//       }
//     },
//     topic: "industry-tech"
//   };

//   return admin
//     .messaging()
//     .send(message)
//     .then(response => {
//       return res.status(200);
//       //     return res.redirect(200, snapshot.ref.toString());
//       //   // Response is a message ID string.
//       //   console.log("Successfully sent message:", response);
//     })
//     .catch(error => {
//       return res.status(500);
//       //   console.log("Error sending message:", error);
//     });

//   //   // Grab the text parameter.
//   //   const original = req.query.text;
//   //   // Push the new message into the Realtime Database using the Firebase Admin SDK.
//   //   return admin
//   //     .database()
//   //     .ref("/messages")
//   //     .push({ original: original })
//   //     .then(snapshot => {
//   //       // Redirect with 303 SEE OTHER to the URL of the pushed object in the Firebase console.
//   //       return res.redirect(303, snapshot.ref.toString());
//   //     });
// });
