import Vue from "vue";
import Vuetify from "vuetify";
import VueWait from "vue-wait";
import Notifications from "vue-notification";
import "@mdi/font/css/materialdesignicons.css";
import "material-design-icons-iconfont/dist/material-design-icons.css";
import "vuetify/dist/vuetify.min.css";
import App from "./App.vue";
import store from "./store";
import router from "./router";

Vue.use(VueWait);
Vue.use(Notifications);
// Vue.use(Vuetify, {
//   iconfont: "mdi"
// });
Vue.use(Vuetify);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  wait: new VueWait({
    useVuex: true
  }),
  render: h => h(App)
}).$mount("#app");
