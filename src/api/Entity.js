export default class Entity {
  constructor(entityCollection, fileRef) {
    this.entityCollection = entityCollection;
    this.fileRef = fileRef;

    this.getPictureRef.bind(this);
    this.uploadPicture.bind(this);
    this.deletePicture.bind(this);
    this.getAll.bind(this);
    this.create.bind(this);
    this.delete.bind(this);
    this.update.bind(this);
  }

  getPictureRef(fileName) {
    return this.fileRef.child(fileName);
  }
  uploadPicture(picture) {
    // TODO: give normal name
    const fileName = `${+new Date()}`;
    const pictureRef = this.getPictureRef(fileName);
    return pictureRef
      .put(picture)
      .then(() => {
        return pictureRef.getDownloadURL();
      })
      .then(url => {
        return { pictureName: fileName, pictureUrl: url };
      });
  }
  deletePicture(fileName) {
    const pictureRef = this.getPictureRef(fileName);
    return pictureRef.delete();
  }
  getAll() {
    return this.entityCollection.get().then(querySnapshot => {
      let entities = [];
      querySnapshot.forEach(doc => {
        entities.push({ ...doc.data(), id: doc.id });
      });
      return entities;
    });
  }
  create(entity) {
    return this.uploadPicture(entity.picture)
      .then(data => {
        return { name: entity.name, description: entity.description, ...data };
      })
      .then(entity => {
        return this.entityCollection.add(entity).then(docRef => {
          return { ...entity, id: docRef.id };
        });
      });
  }
  delete(entity) {
    return this.deletePicture(entity.pictureName).then(() => {
      return this.entityCollection.doc(entity.id).delete();
    });
  }
  update(entity) {
    const { name, description, picture, pictureName, id } = entity;
    let updatedEntity = { name, description };

    if (!(picture instanceof File)) {
      delete entity.picture;
      return this.entityCollection
        .doc(id)
        .update(updatedEntity)
        .then(() => {
          return { ...entity };
        });
    }

    return this.uploadPicture(picture)
      .then(data => {
        updatedEntity = { ...updatedEntity, ...data };
        this.entityCollection.doc(id).update(updatedEntity);
      })
      .then(() => {
        return this.deletePicture(pictureName);
      })
      .then(() => {
        return { ...updatedEntity, id };
      });
  }
}
