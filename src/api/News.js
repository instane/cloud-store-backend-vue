import { db, imagesRef } from "../firebase";
import Entity from "./Entity";

const newsCollection = db.collection("news");
const newsRef = imagesRef.child("news");

class News extends Entity {
  constructor(collection, ref) {
    super(collection, ref)
  }
}

const news = new News(newsCollection, newsRef);

export default news;
