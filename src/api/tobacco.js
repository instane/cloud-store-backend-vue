import { db, imagesRef } from "../firebase";
import Entity from "./Entity";

const tobaccoCollection = db.collection("tobacco");
const tobaccoRef = imagesRef.child("tobacco");

class Tobacco extends Entity {
  constructor(collection, ref) {
    super(collection, ref)
  }
}

const tobacco = new Tobacco(tobaccoCollection, tobaccoRef);

export default tobacco;
