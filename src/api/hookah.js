import { db, imagesRef } from "../firebase";
import Entity from "./Entity";

const hookahCollection = db.collection("hookah");
const hookahRef = imagesRef.child("hookah");

class Hookah extends Entity {
  constructor(collection, ref) {
    super(collection, ref)
  }
}

const hookah = new Hookah(hookahCollection, hookahRef);

export default hookah;
