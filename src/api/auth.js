import { firebase } from "../firebase";

class Auth {
  constructor(firebase, provider) {
    this.firebase = firebase;
    this.provider = provider;

    this.signInWithGoogle.bind(this);
    this.signOut.bind(this);
  }
  signInWithGoogle() {
    // const provider = new firebase.auth.GoogleAuthProvider();
    return this.firebase
      .auth()
      .signInWithPopup(this.provider)
      .then(result => {
        return result.user;
      });
  }
  signOut() {
    return this.firebase.auth().signOut();
  }
}

const provider = new firebase.auth.GoogleAuthProvider();
const auth = new Auth(firebase, provider);

export default auth;
