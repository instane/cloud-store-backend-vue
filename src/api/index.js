import auth from "./Auth";
import hookah from "./Hookah";
import tobacco from "./Tobacco";
import news from "./News";

export { auth, hookah, tobacco, news };
