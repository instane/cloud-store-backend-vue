import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/storage';

const config = {
  apiKey: "AIzaSyBhUPEktWaGsCWZXKxgF-3ymPVJIlwANgU",
  authDomain: "cloud-331b2.firebaseapp.com",
  databaseURL: "https://cloud-331b2.firebaseio.com",
  projectId: "cloud-331b2",
  storageBucket: "cloud-331b2.appspot.com",
  messagingSenderId: "649204349502"
};

const firebaseApp = firebase.initializeApp(config);
const db = firebase.firestore();
const storage = firebase.storage();
const storageRef = storage.ref();
const imagesRef = storageRef.child("images");
db.settings({ timestampsInSnapshots: true });

export { firebase, db, storage, firebaseApp, storageRef, imagesRef };
