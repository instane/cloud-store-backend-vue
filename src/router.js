import Vue from "vue";
import VueRouter from "vue-router";
// import Index from "./pages/Index";
import Hookah from "./pages/Hookah";
import Tobacco from "./pages/Tobacco";
import News from "./pages/News";
import Login from "./pages/Login";
import NotFound from "./pages/NotFound";
import store from "./store";
import notify from "./notify";

Vue.use(VueRouter);

const routes = [
  {
    path: "/login",
    name: "login",
    component: Login
  },
  {
    path: "/index",
    redirect: "/"
  },
  {
    path: "/",
    redirect: "/hookah"
    // name: "index",
    // component: Index,
    // meta: { requiresAuth: true }
  },
  {
    path: "/hookah",
    name: "hookah",
    component: Hookah,
    meta: { requiresAuth: true }
  },
  {
    path: "/tobacco",
    name: "tobacco",
    component: Tobacco,
    meta: { requiresAuth: true }
  },
  {
    path: "/news",
    name: "news",
    component: News,
    meta: { requiresAuth: true }
  },
  {
    path: "*",
    component: NotFound
  }
];

const router = new VueRouter({
  mode: process.env.NODE_ENV === "production" ? "history" : "hash",
  routes
});

router.beforeEach((to, from, next) => {
  if (to.name === "login" && store.getters.isAuthenticated) {
    notify.error("auth", "Вы уже вошли в систему!");
    next({ path: "/" });
  } else if (
    to.matched.some(record => record.meta.requiresAuth) &&
    !store.getters.isAuthenticated
  ) {
    next({ path: "/login", query: { redirect: to.fullPath } });
  } else {
    next();
  }
});

export default router;
