import types from "./mutation-types";
import {
  hookah as hookahApi,
  auth as authApi,
  tobacco as tobaccoApi,
  news as newsApi
} from "../api";

export default {
  signInWithGoogle({ commit }) {
    return authApi.signInWithGoogle().then(user => {
      commit(types.SET_USER, user);
    });
  },
  signOut({ commit }) {
    return authApi.signOut().then(() => {
      commit(types.SET_USER, null);
    });
  },
  getHookas({ commit }) {
    return hookahApi.getAll().then(hookas => {
      commit(types.SET_HOOKAS, hookas);
    });
  },
  addHookah({ commit }, hookah) {
    return hookahApi.create(hookah).then(hookah => {
      commit(types.ADD_HOOKAH, hookah);
    });
  },
  deleteHookah({ commit }, hookah) {
    return hookahApi.delete(hookah).then(() => {
      commit(types.REMOVE_HOOKAH, hookah);
    });
  },
  updateHookah({ commit }, hookah) {
    // console.log(hookah, hookah.picture instanceof File);
    return hookahApi.update(hookah).then(hookah => {
      commit(types.UPDATE_HOOKAH, hookah);
    });
  },
  getTobaccos({ commit }) {
    return tobaccoApi.getAll().then(tobaccos => {
      commit(types.SET_TOBACCOS, tobaccos);
    });
  },
  addTobacco({ commit }, tobacco) {
    return tobaccoApi.create(tobacco).then(tobacco => {
      commit(types.ADD_TOBACCO, tobacco);
    });
  },
  deleteTobacco({ commit }, tobacco) {
    return tobaccoApi.delete(tobacco).then(() => {
      commit(types.REMOVE_TOBACCO, tobacco);
    });
  },
  updateTobacco({ commit }, tobacco) {
    return tobaccoApi.update(tobacco).then(tobacco => {
      commit(types.UPDATE_TOBACCO, tobacco);
    });
  },
  getNews({ commit }) {
    return newsApi.getAll().then(news => {
      commit(types.SET_NEWS, news);
    });
  },
  addNews({ commit }, news) {
    return newsApi.create(news).then(news => {
      commit(types.ADD_NEWS, news);
    });
  },
  deleteNews({ commit }, news) {
    return newsApi.delete(news).then(() => {
      commit(types.REMOVE_NEWS, news);
    });
  },
  updateNews({ commit }, news) {
    return newsApi.update(news).then(news => {
      commit(types.UPDATE_NEWS, news);
    });
  }
};
