import types from "./mutation-types";

export default {
  [types.INCREMENT](state) {
    state.count++;
  },
  [types.SET_USER](state, user) {
    state.user = user;
  },
  [types.SET_HOOKAS](state, hookas) {
    state.hookas = hookas;
  },
  [types.ADD_HOOKAH](state, hookah) {
    state.hookas = [...state.hookas, hookah];
  },
  [types.REMOVE_HOOKAH](state, hookah) {
    state.hookas = state.hookas.filter(e => e !== hookah);
  },
  [types.UPDATE_HOOKAH](state, hookah) {
    const index = state.hookas.findIndex(obj => obj.id === hookah.id);
    state.hookas[index] = hookah;
  },
  [types.SET_TOBACCOS](state, tobaccos) {
    state.tobaccos = tobaccos;
  },
  [types.ADD_TOBACCO](state, tobacco) {
    state.tobaccos = [...state.tobaccos, tobacco];
  },
  [types.REMOVE_TOBACCO](state, tobacco) {
    state.tobaccos = state.tobaccos.filter(e => e !== tobacco);
  },
  [types.UPDATE_TOBACCO](state, tobacco) {
    const index = state.tobaccos.findIndex(obj => obj.id === tobacco.id);
    state.tobaccos[index] = tobacco;
  },
  [types.SET_NEWS](state, news) {
    state.news = news;
  },
  [types.ADD_NEWS](state, news) {
    state.news = [...state.news, news];
  },
  [types.REMOVE_NEWS](state, news) {
    state.news = state.news.filter(e => e !== news);
  },
  [types.UPDATE_NEWS](state, news) {
    const index = state.news.findIndex(obj => obj.id === news.id);
    state.news[index] = news;
  }
};
