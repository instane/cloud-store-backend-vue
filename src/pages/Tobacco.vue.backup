<template>
    <div>
        <template v-if="$wait.is('tobaccos fetching')">
            <v-progress-linear :indeterminate="true"></v-progress-linear>
        </template>
        <template v-else>
            <template v-if="tobaccos.length > 0">
                <v-container grid-list-lg fluid>
                    <v-layout row wrap>
                        <v-flex xs12 offset-sm2 sm8 offset-md0 md4 lg3 v-for="tobacco in tobaccos" :key="tobacco.id">
                            <card :picture="tobacco.pictureUrl" :title="tobacco.name" :text="tobacco.description">
                                <template slot="actions">
                                    <v-btn @click="showChangeDialog(tobacco)" flat color="blue darken-1">Изменить</v-btn>
                                    <v-btn @click="showDeleteDialog(tobacco)" flat color="red darken-1">Удалить</v-btn>
                                </template>
                            </card>
                        </v-flex>
                    </v-layout>
                </v-container>
            </template>
            <template v-else>
                <jumbotron-empty-entities entitie-name="Табаки"/>
            </template>

            <create-dialog v-model="createDialog" entity-type="табак" :cancel-action="hideCreateDialog" :create-action="createTobacco" :wait-create-name="waitCreateName">
                <template slot="body">
                    <v-container grid-list-md>
                        <v-layout wrap>
                            <v-flex xs12>
                                <v-text-field v-model.trim="newTobacco.name" label="Название" required></v-text-field>
                            </v-flex>
                            <v-flex xs12>
                              <v-textarea v-model.trim="newTobacco.description" label="Описание" required></v-textarea>
                                <!-- <v-text-field v-model.trim="newTobacco.description" label="Описание" required></v-text-field> -->
                            </v-flex>
                            <v-flex xs12>
                                <v-text-field v-model="newTobacco.fakeFieldForAutoReset" type="file" label="Изображение" @change.native="createPictureChanged" />
                            </v-flex>
                        </v-layout>
                    </v-container>
                </template>
            </create-dialog>

            <change-dialog v-model="changeDialog" entity-type="табака" :entity-name="tobaccoForChange.oldName" :cancel-action="hideChangeDialog" :change-action="_updateTobacco" :wait-change-name="waitChangeName">
                <template slot="body">
                    <v-container grid-list-md>
                        <v-layout wrap>
                            <v-flex xs12>
                                <v-text-field v-model.trim="tobaccoForChange.name" label="Название" required></v-text-field>
                            </v-flex>
                            <v-flex xs12>
                              <v-textarea v-model.trim="tobaccoForChange.description" label="Описание" required></v-textarea>
                                <!-- <v-text-field v-model.trim="tobaccoForChange.description" label="Описание" required></v-text-field> -->
                            </v-flex>
                            <v-flex xs12>
                                <v-text-field v-model="tobaccoForChange.fakeFieldForAutoReset" type="file" label="Новое изображение" @change.native="changePictureChanged" />
                            </v-flex>
                        </v-layout>
                    </v-container>
                </template>
            </change-dialog>

            <delete-dialog v-model="deleteDialog" entity-type="табак" :entity-name="tobaccoForDelete.name" :cancel-action="hideDeleteDialog" :delete-action="_deleteTobacco" :wait-delete-name="waitDeleteName" />

            <v-fab-transition>
                <v-btn @click="showCreateDialog" v-show="!hidden" color="red" key="add" dark fab fixed bottom right>
                    <v-icon>mdi-plus</v-icon>
                    <v-icon>mdi-close</v-icon>
                </v-btn>
            </v-fab-transition>
        </template>
        <notifications group="tobacco" />
    </div>
</template>

<script>
import { mapState, mapActions } from "vuex";
import Card from "../components/Card";
import MyDialog from "../components/MyDialog";
import DeleteDialog from "../components/DeleteDialog";
import ChangeDialog from "../components/ChangeDialog";
import CreateDialog from "../components/CreateDialog";
import JumbotronEmptyEntities from "../components/JumbotronEmptyEntities";
import logger from "../logger";
import notify from "../notify";

export default {
  data() {
    return {
      newTobacco: {
        name: "",
        description: "",
        picture: {}
      },
      tobaccoForChange: {},
      tobaccoForDelete: {},
      createDialog: false,
      changeDialog: false,
      deleteDialog: false,
      hidden: false,
      waitDeleteName: "tobacco deleting",
      waitChangeName: "tobacco updating",
      waitCreateName: "tobacco creating"
    };
  },
  components: {
    Card,
    "my-dialog": MyDialog,
    DeleteDialog,
    ChangeDialog,
    CreateDialog,
    JumbotronEmptyEntities
  },
  methods: {
    ...mapActions([
      "getTobaccos",
      "addTobacco",
      "deleteTobacco",
      "updateTobacco"
    ]),
    eraseNewTobacco() {
      this.newTobacco = {
        name: "",
        description: "",
        picture: {}
      };
    },
    showCreateDialog() {
      this.eraseNewTobacco();
      this.createDialog = true;
      this.hidden = true;
    },
    hideCreateDialog() {
      this.createDialog = false;
      this.hidden = false;
    },
    showChangeDialog(tobacco) {
      this.tobaccoForChange = {
        ...tobacco,
        oldName: tobacco.name,
        picture: {}
      };
      this.changeDialog = true;
      this.hidden = true;
    },
    hideChangeDialog() {
      this.changeDialog = false;
      this.hidden = false;
    },
    showDeleteDialog(tobacco) {
      this.tobaccoForDelete = tobacco;
      this.deleteDialog = true;
      this.hidden = true;
    },
    hideDeleteDialog() {
      this.deleteDialog = false;
      this.hidden = false;
    },
    _deleteTobacco() {
      this.$wait.start(this.waitDeleteName);
      this.deleteTobacco(this.tobaccoForDelete)
        .then(() => {
          this.hideDeleteDialog();
          this.$wait.end(this.waitDeleteName);
          notify.success("tobacco", "Табак успешно удален!");
        })
        .catch(error => {
          logger.log(error);
          notify.error("tobacco", "Произошла ошибка при удалении табака!");
        });
    },
    _updateTobacco() {
      this.$wait.start(this.waitChangeName);
      this.updateTobacco(this.tobaccoForChange)
        .then(() => {
          this.hideChangeDialog();
          this.$wait.end(this.waitChangeName);
          notify.success("tobacco", "Табак успешно изменен!");
        })
        .catch(error => {
          logger.log(error);
          notify.error("tobacco", "Произошла ошибка при изменении табака!");
        });
    },
    createPictureChanged(e) {
      this.newTobacco.picture = e.target.files[0];
      logger.log(this.newTobacco.picture);
    },
    changePictureChanged(e) {
      this.tobaccoForChange.picture = e.target.files[0];
      logger.log(this.tobaccoForChange.picture);
    },
    createTobacco() {
      this.$wait.start(this.waitCreateName);
      this.addTobacco(this.newTobacco)
        .then(() => {
          this.hideCreateDialog();
          this.$wait.end(this.waitCreateName);
          notify.success("tobacco", "Табак успешно создан!");
        })
        .catch(error => {
          logger.log(error);
          notify.error("tobacco", "Произошла ошибка при добавлении табака!");
        });
    }
  },
  mounted() {
    this.$wait.start("tobaccos fetching");
    this.getTobaccos().then(() => {
      this.$wait.end("tobaccos fetching");
    });
  },
  computed: {
    ...mapState(["tobaccos"])
  }
};
</script>
