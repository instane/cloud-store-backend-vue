import Vue from "vue";

export default {
  error(group, text, title = "Ой-Ей!") {
    Vue.prototype.$notify({
      group,
      title,
      text,
      type: "error"
    });
  },
  success(group, text, title = "Отлично!") {
    Vue.prototype.$notify({
      group,
      title,
      text,
      type: "success"
    });
  }
};
